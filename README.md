# InClassShooter


Risketos Basics:

-Control de personatge amb base ACharacter (Se mueve y hace sus cosas)
-Capacitat de disparar amb Trace line (Raycast)
-Ha d’implementar una petita inteligencia artificial als enemics (Pero de inteligencia poca)
-Usar delegados para alguna acción, ejemplo disparar (Los he utilizado para dropear el arma al suelo)
-Més d’un tipus d'armes i/o bales (Hay 2 armas: -el horsea que dispara una bala y tiene rango de disparo largo y el caracol "Gary" que es una escopeta)
-Ús de les Macros d’Unreal Engine per privatització, escritura i lectura 

Risketos Opcionals:

-Control de jugador avançat (Puedes esprintar con el Shift)
-Shaders (El del Cangrejo por ejemplo, hay unos pocos mas)
-Drop de las armas (Aunque no he podido hacer que las volviera a recoger :( )
-Mapa de Fondo de Bikini porque a todo el mundo le gusta Bob el esponjas
-La otra arma es una escopeta que dispara perdigones en direcciones random (con el raycast)
-He tocado cosas de iluminación y tal pero Unreal es una mierda y casi no se notan
-Portese bien profesor
