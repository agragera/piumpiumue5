// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComp.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UWeaponComp::UWeaponComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWeaponComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponComp::AttachToPlayer(APlayerChar* target){

	selfActor = target;
	if(selfActor){

		FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, true);
		GetOwner()->AttachToComponent(selfActor->GetWeaponPoint(), rules, TEXT("WeaponPoint"));
		
		selfActor->evOnUseItem.AddDynamic(this, &UWeaponComp::Fire);
	}

	
}

void UWeaponComp::Fire(){
	if(!selfActor || !selfActor->GetController())
		return;

	UE_LOG(LogTemp, Warning, TEXT("pium"));

	UWorld* const w = GetWorld();
	UCameraComponent* cam = selfActor->GetCam();

	FCollisionQueryParams inParams;
	inParams.AddIgnoredActor(selfActor);

	FCollisionResponseParams outParams;	

	FHitResult hit;

	if (arma == 1) {
		FVector const vStart = cam->GetComponentLocation();
		FVector const vEnd = vStart + UKismetMathLibrary::GetForwardVector(cam->GetComponentRotation()) * 5000;
		w->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_Camera, inParams, outParams);
		DrawDebugLine(w, vStart, vEnd, hit.bBlockingHit ? FColor::Red : FColor::Blue, false,
			5.f, 0, 10.f);
		UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnd.ToCompactString());

		if (hit.bBlockingHit && IsValid(hit.GetActor())) {
			UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));

			AEnemy* e = Cast<AEnemy>(hit.GetActor());
			if (e) {
				e->Muelto(20);
			}

		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No >:("));
		}
	}
	if (arma == 2) {
		for (size_t i = 0; i < 10; i++)
		{
			int x = FMath::RandRange(-50, 50);
			int z = FMath::RandRange(-50, 50);

			FVector const vStart = cam->GetComponentLocation();
			FVector const vEnd = vStart + UKismetMathLibrary::GetForwardVector(cam->GetComponentRotation()) * 2000 + FVector(x, 0, z);
			w->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_Camera, inParams, outParams);
			DrawDebugLine(w, vStart, vEnd, hit.bBlockingHit ? FColor::Yellow : FColor::Black, false,
				5.f, 0, 10.f);
			UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnd.ToCompactString());

			if (hit.bBlockingHit && IsValid(hit.GetActor())) {
				UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));

				AEnemy* e = Cast<AEnemy>(hit.GetActor());
				if (e) {
					e->Muelto(3);
				}

			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("No >:("));
			}
		}
		
	}
	
}









