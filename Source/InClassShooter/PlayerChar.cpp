// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerChar.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"



// Sets default values
APlayerChar::APlayerChar()
{
	PrimaryActorTick.bCanEverTick = true;

	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCam"));
	cam->SetupAttachment(GetCapsuleComponent());
	cam->bUsePawnControlRotation = true;

	GetMesh()->SetupAttachment(cam);

	hingeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HingeWeapon"));
	hingeMesh->SetupAttachment(cam);
	hingeMesh->bCastDynamicShadow = true;
	hingeMesh->CastShadow = false;
	// hingeMesh->SetRelativeLocation(FVector())
	hingeMesh->SetOnlyOwnerSee(true);

	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->BrakingFriction = 10.f;
	cmove->MaxAcceleration = 10000.f;
	cmove->MaxWalkSpeed = 1000.f;
	cmove->JumpZVelocity = 570.f;
	cmove->AirControl = 2.f;
	charmove = GetCharacterMovement();
}

// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APlayerChar::OnUseItem);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerChar::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerChar::SprintStop);

	PlayerInputComponent->BindAxis("Forward", this, &APlayerChar::MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &APlayerChar::MoveRight);

	PlayerInputComponent->BindAxis("MouseRight", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("MouseUp", this, &APawn::AddControllerPitchInput);

}

void APlayerChar::MoveForward(float vel) {
	if (vel != 0)
		AddMovementInput(GetActorForwardVector(), vel * moveSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("adelante"));
}

void APlayerChar::MoveRight(float vel) {
	if (vel != 0)
		AddMovementInput(GetActorRightVector(), vel * moveSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("andelao"));
}

void APlayerChar::Sprint() {
	charmove->MaxWalkSpeed = 1500.f;
}

void APlayerChar::SprintStop() {
	charmove->MaxWalkSpeed = 1000.f;
}

void APlayerChar::OnUseItem(){
	evOnUseItem.Broadcast();
}

//Funcion para matar al jugador que funciona pero no lo mato porque es un co�azo (y te matan de 1 hit XD)
void APlayerChar::Hit(float dmg) {
	UE_LOG(LogTemp, Warning, TEXT("Ha sido HIT"));
}
